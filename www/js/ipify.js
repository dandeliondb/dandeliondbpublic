  $( document ).on("shiny:sessioninitialized", function getIP() {

  const queryString = 'https://api.ipify.org?format=json';
  fetch(queryString)
    .then(function (response) {
    return response.json();
    })
    .then(function (data) {
      Shiny.setInputValue("LocationIP", data.ip);
    });

  })
  