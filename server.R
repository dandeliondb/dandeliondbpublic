###############################################################################+
# R Shiny App - Server                                                      ####
# Dandelion DB - A civil resistance database
#
# Author: DandelionDB Team
# Date, Init: 2021-02-27
# Contact: dandeliondb[AT]pm.me
#
###############################################################################+

source("./global.R")

function(input, output, session) {
  
  ###########################################################################+
  # Panel "Start" ####
  ###########################################################################+
  
  # validate Input

  validateGetActionDataStart <- reactive({

    view <- "ddb_versions_latest"
    filter.id <- getFilterID()

    shiny::validate(
      needDDB(class(getActionDataTable(view = view,
                                    filter.id = filter.id,
                                    filter = start.map.static.greta.filter)) == "data.frame",
           showNotification("Action data for static map must be provided!"))
    )

    start.data <-  getActionDataTable(view = view,
                                      filter.id = filter.id,
                                      filter = start.map.static.greta.filter)
    
    vars.needed <- c("ActionDate", "LocationLongitude", "LocationLatitude", "Activists")
    valid.tf <- complete.cases(start.data[, vars.needed])

    start.query.data <- list(view = view,
                             fid = filter.id,
                             filter = start.map.static.greta.filter,
                             data =  start.data[valid.tf,])
    
    return(start.query.data)

    })

  ## show Static Map

  observeEvent({input$tabs=="Start"}, {

    start.query.data <- validateGetActionDataStart()

    start.map.static <- showStaticActionMap(actions.df = start.query.data[["data"]],
                                            view = start.query.data[["view"]],
                                            filter.id = start.query.data[["fid"]],
                                            filter = start.query.data[["filter"]])

    output$StartMapStaticSubtitle <- renderText({start.map.static[["sub.title"]]})

    output$StartMapStatic <- renderLeaflet({start.map.static[["map"]]})

  })

  ###########################################################################+
  # Panel "Enter Action" ####
  ###########################################################################+
  
  validatePreviousActionInstanceIdVersionEnter <- reactive({

    shiny::validate(
    needDDB(validateActionInstanceIdVersion(action.instance.id = input$PreviousActionInstanceId,
                                            action.instance.version = input$PreviousActionInstanceVersion),
         showNotification("Oops, action (ActionInstanceId or ActionInstanceVersion) not found in DandelionDB!")
    ))

    aid.aiv.previous <- list(aid= input$PreviousActionInstanceId, aiv = input$PreviousActionInstanceVersion)
    
    return(aid.aiv.previous)

    })


    observeEvent(input$getInfoFromPreviousActionButtonFlag, {

      aid.aiv.previous <- validatePreviousActionInstanceIdVersionEnter()

      action.df <- getActionInstanceData(action.instance.id = aid.aiv.previous[["aid"]],
                                         action.instance.version = aid.aiv.previous[["aiv"]],
                                         filter.id = 0)

      updateEnterActionFromPreviousAction(action.df = action.df)

      })

    observeEvent(input$resetEnterActionButtonFlag,{ resetEnterAction() })


    observeEvent(input$getLocationFromIPButtonFlag, {

      # get User Geolocation

      res  <- GET(paste0("http://api.ipstack.com/",
                         input$LocationIP,"?access_key=",
                         config::get("ipstackkey", file = config.file )))
      data <- fromJSON(rawToChar(res$content))

      # Update input values

      updateTextInput(inputId="LocationIP",
                      label = "(INTERNET ADDRESS AS IP?) [IP NOT SAVED!]",
                      value=ifelse(is.null(data$ip), "Missing", data$ip))

      updateTextInput(inputId="LocationCity",
                      label="CITY?",
                      value=ifelse(is.null(data$city), "Missing", data$city))

      updateTextInput(inputId="LocationCountry" ,
                      label="COUNTRY?",
                      value=ifelse(is.null(data$country_name), "Missing", data$country_name))

      updateTextInput(inputId="LocationContinent",
                      label="CONTINENT? ",
                      value=ifelse(is.null(data$continent_name), "Missing", data$continent_name))

      updateNumericInput(inputId="LocationLatitude",
                         label = "LATITUDE? [South-North, in decimal degrees, for action map, https://gps-coordinates.org]" ,
                         value=ifelse(is.null(data$latitude), NA, data$latitude),
                         min = -90 , max = +90)

      updateNumericInput(inputId="LocationLongitude",
                         label = "LONGITUDE? [East-West, in decimal degrees, for action map, https://gps-coordinates.org]",
                         value=ifelse(is.null(data$longitude), NA, data$longitude),
                         min = -180, max = +180)

      updateSelectInput(inputId= "LocationSource",
                  label = "SOURCE OF LOCATION?",
                  choices = c("Manual", "Automatic (IP)", "Missing"),
                  selected = "Automatic (IP)")

      rm(list=c("res", "data"))

      })


    observeEvent(input$saveNewActionButtonFlag,{

      ActionInstanceId <- ifelse(isActionInstanceIdinDB(input$ActionInstanceId),
             as.integer(input$ActionInstanceId), "<NewActionInstance>")
      
      ActionSeriesId <- ifelse(isActionInstanceIdinDB(input$ActionSeriesId),
             as.integer(input$ActionSeriesId), "<NewActionSeries>")
      
      if(        ActionInstanceId == "<NewActionInstance>" & ActionSeriesId == "<NewActionSeries>" ){
        # Case 1: Add new action instance in an new action series => New instance, new series"
        ActionInstanceId      <- getNewActionInstanceId()
        ActionSeriesId        <- ActionInstanceId
        ActionInstanceVersion <- 1
      } else if( ActionInstanceId == "<NewActionInstance>" & ActionSeriesId != "<NewActionSeries>" ){
        # Case 2: Add new action instance in an old action series => "New instance, old series"
        ActionInstanceId      <- getNewActionInstanceId()
        ActionSeriesId        <- as.integer(ActionSeriesId)
        ActionInstanceVersion <- 1
      } else if( ActionInstanceId != "<NewActionInstance>" & ActionSeriesId != "<NewActionSeries>" ){
        # Case 4: Update old action in an old action series => "New version, old instance, old series"
        ActionInstanceId      <- as.integer(ActionInstanceId)
        ActionSeriesId        <- as.integer(ActionSeriesId)
        ActionInstanceVersion <- getNewActionInstanceVersion(action.instance.id = as.integer(ActionInstanceId))
      } else if( ActionInstanceId != "<NewActionInstance>" & ActionSeriesId == "<NewActionSeries>" ){
        # Case 3: Assign old action into a new action series => "New version, old instance, new series"
        # Starting a new series from an old instance works except for an initial instance, which defines the series id
        ActionInstanceId      <- as.integer(ActionInstanceId)
        ActionSeriesId        <- ActionInstanceId
        ActionInstanceVersion <- getNewActionInstanceVersion(action.instance.id = as.integer(ActionInstanceId))
      } 

      ddb.new.df <- data.frame(
          ActionSeriesId    = as.integer(ActionSeriesId),
          ActionInstanceId  = as.integer(ActionInstanceId),
          ActionInstanceVersion = as.integer(ActionInstanceVersion),
          LatestVersion     = as.integer(ActionInstanceVersion),
          ActionDate        = as.character(input$ActionDate),
          TimeStart         = as.character(input$TimeStart),
          TimeStop          = as.character(input$TimeStop),
          ActionTitle       = input$ActionTitle,
          ActionDescription = input$ActionDescription,
          ActionDemand      = input$ActionDemand,
          ActionTagsIssue   = paste0(input$ActionTagsIssue, collapse=", "),
          TypeOfAction      = paste0(input$TypeOfAction, collapse=", "),
          ActionLevels      = paste0(input$ActionLevels, collapse=", "),
          GeneSharpLevelA   = paste0(input$GeneSharpLevelA, collapse=", "),
          GeneSharpLevelB   = paste0(input$GeneSharpLevelB, collapse=", "),
          GeneSharpLevelC   = paste0(input$GeneSharpLevelC, collapse=", "),
          Affiliations      = paste0(input$Affiliations, collapse=", "),
          Activists         = input$Activists,
          ActivistsSource   = paste0(input$ActivistsSource, collapse=","),
          ActivistsSourceLink= paste0(input$ActivistsSourceLink, collapse=", "),
          LocationLongitude = input$LocationLongitude,
          LocationLatitude  = input$LocationLatitude,
          LocationStreet    = input$LocationStreet,
          LocationStreetNr  = input$LocationStreetNr,
          LocationPostcode  = input$LocationPostcode,
          LocationCity      = input$LocationCity,
          LocationCountry   = input$LocationCountry,
          LocationContinent = input$LocationContinent,
          LocationSource    = input$LocationSource,
          MediaLink         = input$MediaLink,
          InfoLink          = input$InfoLink,
          TimeStampUTC      = as.character(as.POSIXlt(Sys.time(), tz="UTC")),
          AppVersion        = app.version
          )

        createNewAction(ddb.new.df)

        Sys.sleep(0.5) # Protect user from unwanted multiple entries!

        showNotification(paste0("ActionSeriesId <", ActionSeriesId ,">, ",
                                "ActionInstanceId <", ActionInstanceId,"> and ",
                                "ActionInstanceVersion <", ActionInstanceVersion,"> ",
                                "was saved!"),
                         duration=10, closeButton = TRUE,
                         type="message")

    })

    ###########################################################################+
    # Panel "Show DataSheet" ####
    ###########################################################################+

    # validate Input

    validatePreviousActionInstanceIdVersionSheet <- reactive({
      
      datasheet.filter.id <- getFilterID()

      shiny::validate(
        needDDB(validateActionInstanceIdVersion(action.instance.id = input$SheetActionInstanceId,
                                                action.instance.version = input$SheetActionInstanceVersion),
             showNotification("Oops, action (ActionInstanceId or ActionInstanceVersion) not found in DandelionDB!"))
      )

      datasheet.data <- getActionInstanceData(action.instance.id = input$SheetActionInstanceId,
                                                 action.instance.version = input$SheetActionInstanceVersion,
                                                 filter.id = datasheet.filter.id)
      
      datasheet.query.data <-list(view = "ddb_versions_all",
                                  fid = datasheet.filter.id,
                                  filter  = paste("WHERE ",
                                                  "ActionInstanceId = ", input$SheetActionInstanceId, " AND ",
                                                  "ActionInstanceVersion = ", input$SheetActionInstanceVersion, " AND ",
                                                  "TimeStampUTC IS NOT NULL"),
                                  data=datasheet.data)

      
      return(datasheet.query.data)

   })

    # show Datasheet

    observeEvent(input$tabs == "ShowDatasheet" | input$showDatasheetButtonFlag, {
      
      datasheet.query.data <<- validatePreviousActionInstanceIdVersionSheet()
      
        output$datasheet <- renderUI({
          isolate(HTML(markdown::markdownToHTML(
            knit(input= "./templates/res_datasheet.Rmd",
                 output=  paste0("./archive/dandeliondb_datasheet",
                                 "_aid_", datasheet.query.data[["data"]][1, "ActionInstanceId"],
                                 "_aiv_", datasheet.query.data[["data"]][1, "ActionInstanceVersion"],
                                 "_fid_", datasheet.query.data[["fid"]],
                                 ".html"),
                 quiet = FALSE),
            fragment.only=TRUE)))
        })
    })

    output$downloadActionSheet <- downloadHandler(
      filename = function() {
        paste0("dandeliondb_datasheet",
               "_aid_", datasheet.query.data[["data"]][1, "ActionInstanceId"],
               "_aiv_", datasheet.query.data[["data"]][1, "ActionInstanceVersion"],
               "_fid_", datasheet.query.data[["fid"]],
               ".csv")
      },
      content = function(file) {
        action.df <- getActionInstanceDataSheet(action.df =  datasheet.query.data[["data"]])
        writeDataToTabDelFile(data=action.df, file=file)
      }
    )

    ###########################################################################+
    # Panel "Show Table" ####
    ###########################################################################+
    
    # validate Input

    validateGetActionDataTable <- reactive({
      
      trigger1 <- observeTabsShowTable()
      trigger2 <- observeFilterDataTableButtonFlag()

      table.filter.id <- getFilterID()
      
      shiny::validate(
        needDDB(class(getActionDataTable(view = input$ShowTableView,
                                         filter.id = table.filter.id,
                                         filter = input$ActionsTableFilter)) == "data.frame",
             showNotification("Oops, this filter is not working in DandelionDB!"))
        )

      table.data <- getActionDataTable(view = input$ShowTableView,
                                       filter.id = table.filter.id,
                                       filter = input$ActionsTableFilter)
      
      table.query.data <- list(view = input$ShowTableView,
                               fid = table.filter.id,
                               filter = input$ActionsTableFilter,
                               data = table.data)
      
      return(table.query.data)

    })

    # show Table

    observeTabsShowTable <- reactive({ input$tabs == "ShowTable"})
    observeFilterDataTableButtonFlag <- reactive({ input$filterDataTableButtonFlag })

    observeEvent({input$tabs == "ShowTable" | input$filterDataTableButtonFlag}, {
      
      table.query.data <<- validateGetActionDataTable()
      
      output$TableFilterID <- renderText({paste0("Filter ID (FID): ", table.query.data[["fid"]])})
      output$TableFilterExample1 <- renderText({"Example 1: ActionDate = '2020-05-15'"})
      output$TableFilterExample2 <- renderText({"Example 2: ActionDate BETWEEN '2020-05-15' AND ActionDate <= '2020-06-15'"})
      output$TableFilterExample3 <- renderText({"Example 3: Affiliations LIKE '%Fridays%' OR Affiliations LIKE '%Extinction%'"})
      output$TableFilterNote     <- renderText({"Further Info: https://www.mariadbtutorial.com/mariadb-basics/mariadb-where/"})

      output$table <- DT::renderDataTable({DT::datatable( table.query.data[["data"]],
                                                          rownames = FALSE)})
      
      output$TableLegend1 <- renderText({"NOTE 1: The table shows only actions with a valid time stamp (TimeStampUTC)."})
      output$TableLegend2 <- renderText({"NOTE 2: Update table to see on-going updates."})
      output$TableLegend3 <- renderText({"NOTE 3: More information on how to use 'Search:' window above at <https://datatables.net/reference/option/searching>."})

    })

    output$downloadFilteredDataTable <- downloadHandler(
      filename = function() {
        tolower(paste0("dandeliondb_data",
                       "_ts_", gsub(Sys.time(), pattern=" |\\:|-|<|>", replacement = ""),
                       "_fid_", table.query.data[["fid"]], ".csv"))
      },
      content = function(file) {
        writeDataToTabDelFile(data=table.query.data[["data"]], file=file)
      }
    )

    ###########################################################################+
    # Panel "Show Summary" ###
    ###########################################################################+

    # validate input

    validateGetActionDataSummary <- reactive({
      
      trigger1 <- observeTabsShowSummary()
      trigger2 <- observeFilterDataSummaryButtonFlag()

      summary.filter.id <- getFilterID()

      shiny::validate(
        needDDB(class(getActionDataTable(view =  input$ShowSummaryView,
                                      filter.id = summary.filter.id,
                                      filter =input$ActionsSummaryFilter)) == "data.frame",
             showNotification("Oops, this filter is not working in DandelionDB!"))
      )

      summary.data <- getActionDataTable(view =  input$ShowSummaryView,
                                         filter.id = summary.filter.id,
                                         filter = input$ActionsSummaryFilter)

      summary.query.data <- list(view =  input$ShowSummaryView,
                                 fid = summary.filter.id,
                                 filter = input$ActionsSummaryFilter,
                                 data = summary.data)
      
      return(summary.query.data)

    })

    # show Summary

    observeTabsShowSummary <- reactive({ input$tabs == "ShowSummary" })
    observeFilterDataSummaryButtonFlag <- reactive({ input$filterDataSummaryButtonFlag })
    
    observeEvent( input$tabs == "ShowSummary" | input$filterDataSummaryButtonFlag, {

      summary.query.data <<- validateGetActionDataSummary()

      output$SummaryFilterID <- renderText({paste0("Filter ID (FID): ", summary.query.data[["fid"]])})
      output$SummaryFilterExample1 <- renderText({"Example 1: ActionDate = '2020-05-15'"})
      output$SummaryFilterExample2 <- renderText({"Example 2: ActionDate BETWEEN '2020-05-15' AND ActionDate <= '2020-06-15'"})
      output$SummaryFilterExample3 <- renderText({"Example 3: Affiliations LIKE '%Fridays%' OR Affiliations LIKE '%Extinction%'"})
      output$SummaryFilterNote     <- renderText({"Further Info: https://www.mariadbtutorial.com/mariadb-basics/mariadb-where/"})

      output$summary <- renderUI({
          isolate(HTML(markdown::markdownToHTML(
            knit(input =paste0('./templates/res_summary.Rmd'),
                 output=tolower(paste0("./archive/dandeliondb_summary",
                                       "_ts_", gsub(Sys.time(), pattern=" |\\:|-|<|>", replacement = ""),
                                       "_fid_", summary.query.data[["fid"]], ".html")),
                 quiet = FALSE),
            fragment.only=TRUE)
          ))
        })

      })

    output$downloadFilteredDataSummary <- downloadHandler(
      filename = function() {
        tolower(paste0("dandeliondb_summary",
                       "_ts_", gsub(Sys.time(), pattern=" |\\:|-|<|>", replacement = ""),
                       "_fid_", summary.query.data[["fid"]], ".csv"))
        },
      content = function(file) {
        actions.df <- summary.query.data[["data"]]
        summary.df <- getActionsSummary(actions.df = actions.df,
                                        view =  summary.query.data[["view"]],
                                        filter.id = summary.query.data[["fid"]],
                                        filter = summary.query.data[["filter"]])
        writeDataToTabDelFile(data=summary.df, file=file)
        }
    )

    ###########################################################################+
    # Panel "Show Map (Static)" ###
    ###########################################################################+

    # validate Input

    validateGetActionDataMapStatic <- reactive({
      
      trigger1 <- observeTabsShowMapStatic()
      trigger2 <- observeFilterDataMapStaticButtonFlag()

      map.static.filter.id <- getFilterID()

      shiny::validate(
        needDDB(class(getActionDataTable(view = input$ShowMapStaticView,
                                      filter.id = map.static.filter.id,
                                      filter = input$ActionsMapStaticFilter)) == "data.frame",
             showNotification("Ooops, this filter is not working in DandelionDB!"))
      )

      map.static.data <- getActionDataTable(view = input$ShowMapStaticView,
                                            filter.id = map.static.filter.id,
                                            filter = input$ActionsMapStaticFilter)

      vars.needed <- c("ActionDate", "LocationLongitude", "LocationLatitude", "Activists")
      valid.tf <- complete.cases(map.static.data[, vars.needed])

      map.static.query.data <- list(view = input$ShowMapStaticView,
                                    fid = map.static.filter.id,
                                    filter = input$ActionsMapStaticFilter,
                                    data = map.static.data[valid.tf, ])
      
      return(map.static.query.data)
      
    })

    # show Map (Static)
    
    observeTabsShowMapStatic <- reactive({ input$tabs == "ShowMapStatic"  })
    observeFilterDataMapStaticButtonFlag <- reactive({ input$filterDataMapStaticButtonFlag })

    observeEvent(input$tabs == "ShowMapStatic" | input$filterDataMapStaticButtonFlag, {

      map.static.query.data  <<- validateGetActionDataMapStatic()

      output$MapStaticFilterID <- renderText({paste0("Filter ID (FID): ", map.static.query.data[["fid"]])})
      output$MapStaticFilterExample1 <- renderText({"Example 1: ActionDate = '2020-05-15'"})
      output$MapStaticFilterExample2 <- renderText({"Example 2: ActionDate BETWEEN '2020-05-15' AND ActionDate <= '2020-06-15'"})
      output$MapStaticFilterExample3 <- renderText({"Example 3: Affiliations LIKE '%Fridays%' OR Affiliations LIKE '%Extinction%'"})
      output$MapStaticFilterNote     <- renderText({"Further Info: https://www.mariadbtutorial.com/mariadb-basics/mariadb-where/"})

      output$MapStatic <- renderLeaflet({
        showStaticActionMap(view = map.static.query.data[["view"]],
                            filter.id = map.static.query.data[["fid"]],
                            filter = map.static.query.data[["filter"]])[["map"]]
        })
    })

    ###########################################################################+
    # Panel "Show Map (Dynamic)" ###
    ###########################################################################+

    # validate Input

    validateGetActionDataMapDynamic <- reactive({
      
      trigger1 <- observeTabsShowMapStatic()
      trigger2 <- observeFilterDataMapDynamicButtonFlag()
      
      map.dynamic.filter.id <- getFilterID()

      shiny::validate(
        needDDB(class(getActionDataTable(view = input$ShowMapDynamicView,
                                      filter.id = map.dynamic.filter.id,
                                      filter = input$ActionsMapDynamicFilter)) == "data.frame",
             showNotification("Oops, this filter is not working in DandelionDB!"))
      )

      map.dynamic.data <- getActionDataTable(view =  input$ShowMapDynamicView,
                                             filter.id = map.dynamic.filter.id,
                                             filter = input$ActionsMapDynamicFilter)

      vars.needed <- c("ActionDate", "LocationLongitude", "LocationLatitude", "Activists")
      valid.tf <- complete.cases(map.dynamic.data[, vars.needed])

      map.dynamic.query.data <- list(view = input$ShowMapDynamicView,
                                     fid = map.dynamic.filter.id,
                                     filter = input$ActionsMapDynamicFilter,
                                     data = map.dynamic.data[valid.tf, ])

      return(map.dynamic.query.data)
      
    })

    # show Map (Dynamic)
    
    observeTabsShowMapStatic <- reactive({ input$tabs == "ShowMapDynamic"  })
    observeFilterDataMapDynamicButtonFlag <- reactive({ input$filterDataMapDynamicButtonFlag })

    observeEvent(input$tabs == "ShowMapDynamic" | input$filterDataMapDynamicButtonFlag, {

      map.dynamic.query.data  <<- validateGetActionDataMapDynamic()

      output$MapDynamicFilterID <- renderText({paste0("Filter ID (FID): ", map.dynamic.query.data[["fid"]])})
      output$MapDynamicFilterExample1 <- renderText({"Example 1: ActionDate = '2020-05-15'"})
      output$MapDynamicFilterExample2 <- renderText({"Example 2: ActionDate BETWEEN '2020-05-15' AND ActionDate <= '2020-06-15'"})
      output$MapDynamicFilterExample3 <- renderText({"Example 3: Affiliations LIKE '%Fridays%' OR Affiliations LIKE '%Extinction%'"})
      output$MapDynamicFilterNote     <- renderText({"Further Info: https://www.mariadbtutorial.com/mariadb-basics/mariadb-where/"})

      updateDateRangeInput(inputId = "MapDynamicDateRange",
                           label = "RANGE OF DATES?",
                           start = min( map.dynamic.query.data[["data"]][["ActionDate"]], na.rm=TRUE),
                           end   = max( map.dynamic.query.data[["data"]][["ActionDate"]], na.rm=TRUE),
                           min = Sys.Date() - 5*365, max = Sys.Date())
      
      output$MapDynamic <- renderLeaflet({
        showStaticActionMap(view = map.dynamic.query.data[["view"]],
                            filter.id = map.dynamic.query.data[["fid"]],
                            filter = map.dynamic.query.data[["filter"]])[["map"]]
      })
    })

    updateMapDynamicDay <- reactive({input$MapDynamicDay})

    updateMapDynamicRange <- reactive({input$MapDynamicDateRange})

    observeEvent(input$MapDynamicDay, {

      showDynamicActionMap(map.id = "MapDynamic",
                           actions.df =  map.dynamic.query.data[["data"]],
                           view = map.dynamic.query.data[["view"]],
                           filter.id = map.dynamic.query.data[["fid"]],
                           filter = isolate(map.dynamic.query.data[["filter"]]),
                           date.current = isolate(updateMapDynamicDay()))
    })

    output$MapDynamicDateSlider <-renderUI({

    action.dates <-  sort(unique(map.dynamic.query.data[["data"]][["ActionDate"]]))
    action.dates <- action.dates[action.dates >= updateMapDynamicRange()[1] &
                                 action.dates <= updateMapDynamicRange()[2] ]

    shinyWidgets::sliderTextInput(inputId = "MapDynamicDay",
                                  label  = "SHOW ACTIONS ON WHICH DATE? ",
                                  choices = action.dates,
                                  width = width.default,
                                  animate = animationOptions(interval    = 500, # msec
                                                             loop        = TRUE,
                                                             playButton  = "PLAY",
                                                             pauseButton = "PAUSE"))
    })


    ###########################################################################+
    # Panel "Global Crises" ####
    ###########################################################################+

    output$globalcrises <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_globalcrises.Rmd',
                                         output='./archive/doc_globalcrises.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })


    ###########################################################################+
    # Panel "Civil Resistance" ####
    ###########################################################################+
    
    output$civilresistance <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_civilresistance.Rmd',
                                         output='./archive/doc_civilresistance.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })
    
    ###########################################################################+
    # Panel "DandelionDB Tutorials" ####
    ###########################################################################+
    
    output$tutorials <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_tutorials.Rmd',
                                         output='./archive/doc_tutorials.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })

    ###########################################################################+
    # Panel "DandelionDB Design" ####
    ###########################################################################+
    
    output$design <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_design.Rmd',
                                         output='./archive/doc_design.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })

    ###########################################################################+
    # Panel "DandelionDB Version" ####
    ###########################################################################+
    
    output$version <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_version.Rmd',
                                         output='./archive/doc_version.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })

    ###########################################################################+
    # Panel "Disclaimer" ####
    ###########################################################################+
    
    output$disclaimer <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_disclaimer.Rmd',
                                         output='./archive/doc_disclaimer.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })

    ###########################################################################+
    # Panel "Contact" ####
    ###########################################################################+
    
    output$contact <- renderUI({
      HTML(markdown::markdownToHTML(knit(input= './templates/doc_contact.Rmd',
                                         output='./archive/doc_contact.html',
                                         quiet = FALSE),
                                    fragment.only=TRUE))
    })
    

    ###########################################################################+
    # Clean up ####
    ###########################################################################+
    
    on.exit(dbDisconnect(con))
}

##EOF##########################################################################+