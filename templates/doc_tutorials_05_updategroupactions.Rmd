---
title: "DDB Tutorials - Enter Action"
author: "DandelionDB Team"
date: "2021-02-27"
output: html_document
---

# 2.5 DandelionDB - Updating and Grouping Actions

Hallo, this is a short tutorial on the DandelionDB database. Here, I will explain 
how re-use data from a previous action, update an action or group multiple actions
to a series!

## Background

DandelionDB was designed with the objective to provide a simple system for version 
control and grouping of actions. 

The reason is that grass-roots protests or civil resistance actions are often 
decentralized, dynamic, and not strictly organized. This means, activists do usually 
not have access to the latest or "official" numbers of locations and number of protesters of 
collective events. As a consquence reporting summary statistics on actions requires 
multiple updates and grouping of local actions which are then aggregated to a 
global action, for example, a global climate action week.

Therefore, in DandelionDB single actions have "action instance identifiers" and
"action series identifiers" which per default are identical. However, by assigning 
the same "action series identifier" to multiple action instances one can create 
an action series. The shared "action series identifier" is usually defined by 
the first action initiating an action series.

## How do you enter the info of an action?

You can enter the complete updated info on a previous event manually,
which is simple, but tedious (see also tutorial "Enter Action"). 

Alternatively, you can get the info of a similar previous action from the database and
load it into the current web form. To do this, you only have to enter the "action
instance identifier" (ActionInstanceId) and the "action instance version" 
(ActionInstanceVersion) into the fields at the top of the web form and press the 
"Get Info" button. 

If you don't know what the "action instance identifier" of the previous action is,
you will have to go to the "Show Actions" panel, select "Show Table", filter or 
quick-search the table of actions till you have found the right action instance and
right action instance version. Per default only the latest action instance version
is shown, but you can also choose to show all action instance versions. 

If you have found the right action instance and version, you can copy the
"action series identifier", "the action instance identifier", and the "action instance version"
into the clipboard and paste into into the fields at the top of the "Enter Action"
panel. Please note, that you can also just enter "<LatestVersion>" in the action 
instance  version field to select the latest version. The action series identifier
is not yet needed here but later below, so keep the info in the clipboard.

After that you can update only the bits of information that you want to change 
and carry over all other details in the "Required Info" and "Additional Info" sections.

## How do you add, update, or group actions?

Finally, you will come to the "Expert Info" section where the real magic happens:

Case 1) "New Series": If you want to **create a complete new action instance with its own new action series**,
just leave the default entries "<NewActionSeries>" and "<NewActionInstance>" as they are. This
is the recommended default for using DandelionDB.

Then, simply press the "Save" button and a new action instance will be saved. 
A pop-up window will appear which will confirm that you have entered a new action
to the database and display the action series and action instance identifiers, 
so you can more easily find it again.

As an **example**, let's say activists have created a database to collect information on
industry lobby contacts of politicians which they call **DandeTigerDB**. They should go the
"Enter Action" panel fill in the required info - I am skipping some info here for
the sake of time - leave the default values of <NewActionSeries> and <NewActionInstance>
untouched, and press "Save". After receiving the confirmation pop-up window,
one can then go the "Show Table" panel and search for the new entry.

Case 2) "New Instance": If you want to **add a new action instance to an existing action series**,
let's say, a new "Fridays For Future" protest in your local town, then just enter 
the "action series identifier" and leave "<NewActionInstance>" in the other field.
The "action series identifier" is usually the "action instance identifier" of the
first action iniating the series. 

[Then, simply press the "Save" button and a new action instance will be saved. 
A pop-up window will appear which will confirm that you have entered a new action
to the database and display the action series and action instance identifiers, 
so you can more easily find it again.]

As an **example**, let's say italian activists have created an Italian language frontend
web app to connect to the backend database of DandelionDB, which they called **DenteDiLeoneDB**. 
Therefore, it makes sense to include this web app in the same action series as
DandelionDB but treat it as a new action. So one can load the details of the old
DandelionDB into the web form, but change a few crucial details. Then one can
enter the same action series id as DandelionDB, but leave <NewActionInstance> to
treat it as a new action with its own identifier.

Case 3) "New Version": If you want to **add an updated version of a previous action**
while keeping its current assignment to an action series, you have to enter the previous
"action series identifier" and the previous "action instance identifier". 

[Then, simply press the "Save" button and a new version of the action with all 
the updated info from the web form will be saved. A pop-up window will appear
which will confirm that you have entered a new action to the database and display
the action series and action instance identifiers, so you can more easily find it again.]

As an **example**, let's say we just want to update DandelionDB because there was an
error in the info or a **new upgraded version of DandelionDB** was released. Then
we can just load the previous info from DandelionDB into the web form, update
the DandelionDb title and description, go down to "Expert Info" and enter both
the previous action series identifier and the action instance identifier. A new
version number will be assigned automatically.

Case 4) "Update Series": The last case is the least relevant, but mentioned for 
completeness. If you want to  **assign a previous action to a new action series*",
you can just enter the action instance identifier, but leave "<NewActionSeries>" 
in the other field for the action series. Then, a new action series will be started,
using the action instance identifier as the action series identifier. Please note
that this does not work for action instances which are the initial actions of an
action series and define the action series id of subsequent actions.

[Then, simply press the "Save" button and a new version of the previous action 
in a new action series  will be saved. A pop-up window will appear
which will confirm that you have entered a new action to the database and display
the action series and action instance identifiers, so you can more easily find it again.]

As an example, let's say the Italian version of DandelionDB called DenteDiLeoneDB
has developed over time has gotten its own database backend with a different objective.
In this case the developer may feel that the DenteDiLeoneDB should no longer 
be in the same action series as the initial DandelionDB database. Then, he 
could just enter the action instance identifier of DenteDiLeoneDB, but leave 
<NewActionSeries> in the field for Action Series Identifier. After pressing 
the "Save" Button, a new action series with the previous DandelionDB will be 
started.

I hope these instructions did not sound too complicated, just remember the 
purpose of it is just to be able to update actions and group actions together.

Thank you for your interest in DandelionDB! Take care! :-)




