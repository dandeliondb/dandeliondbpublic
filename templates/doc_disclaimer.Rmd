---
title: "Disclaimer"
author: "DandelionDB Team"
date: "2021-02-27"
output: html_document
---

If you require any more information or have any questions about our site's disclaimer, please feel free to contact us by email at [dandeliondb@pm.me](mailto: dandeliondb@pm.me). Our Disclaimer was generated with the help of the [Disclaimer Generator website](https://www.disclaimer-generator.com/).

## Disclaimers for DandelionDB

**"DandelionDB" makes all reported data publicly available in an open-access database**. Therefore, sensitive information which may put the reporting activist or others at risk, should only be reported if the activist and other affected activists are aware of the risks and accept them. The reporting individual is primarily responsible for protecting himself and others (“As private as you make it!”) and, therefore, the DandelionDB Team does not take any responsibility for the information which was contributed by its users.

All the information on this website - http://dandeliondb.info - is published in good faith and for general information purpose only. DandelionDB does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website (DandelionDB), is strictly at your own risk. DandelionDB will not be liable for any losses and/or damages in connection with the use of our website.

From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'.

Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.

## Consent

By using our website, you hereby consent to our disclaimer and agree to its terms.

## Update

Should we update, amend or make any changes to this document, those changes will be prominently posted here.

Last updated: 2021-06-20, DandelionDB Team