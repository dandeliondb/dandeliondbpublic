---
title: "DDB Tutorials - Overview"
author: "DandelionDB Team"
date: "2021-02-27"
output: html_document
---

# 2.1 DandelionDB - Overview

Hallo, this an overview on the DandelionDB website. DandelionDB is a "global, open-access database on civil-resistance actions for science, democracy and a livable future."

## Start

On the **"Start"** tab, you will find a map of the school strikes for the climate by Greta Thunberg as an example.

## Enter Action

On the **"Enter Action"** tab, you can enter the details for an action that you want to contribute to DandelionDB. It includes various features to save you time while entering the details of an action. For example, you can load the details of a previous action into the web form or you can geolocate yourself and fill in your location data automatically. Please note that your internet address will only be used temporarily and not stored at DandelionDB or other providers used for geolocation.

## Show Actions

On the **"Show Actions"** tab, you can choose various ways of displaying the data in DandelionDB.

By selecting **"Show Datasheet"** and entering an identifier and a version for a single action you can display its details in a tabular format.

By selecting **"Show Table"** you can show all data entries in DandelionDB in a table format, apply filters to select specific actions you are interested in, and sort and quickfilter in the resulting table.

By selecting **"Show Summary"** you can display a summary of a group of actions reporting the number of activists, number of actions, number and list of cities, number and list of countries, and the number and list of continents or oceans. A filter can be used to select only those actions you are interested in. This summary should be very useful for organizers or journalists who want to write press releases or articles about actions.

By selecting **"Show Map (Static)"** you can display actions and the number of activists based on their geographic location. A filter can be used to select only those actions you are interested in. By clicking on a specific action, a pop-up will with an action instance id will appear which you can then use to display more information using "Show Datasheet".

By selecting **"Show Map (Dynamic)"** you can display actions and the number of activists based on their geographic location over time, resulting in a sort of animated movie. A filter can be used to select only those actions you are interested in. A range of dates can be set via a calendar sliderbar. The animation can be started and stopped by pressing he "Play" and "Pause" links.

## Documentation

On the **"Show Documentation"* tab, you can get general information on present  human-made global crises that DandelionDB aims to help to solve and an overview on the spectrum of civil resistance actions, which can be used to achieve system change. Additionally, you can find specific information about DandelionDB and how to use it in the tutorials, information about the theoretical and technical design of DandelionDB and the current version. The disclaimer contains important information about the limited liability of DandelionDB as an open-access database, who makes all entered information publicly available. Therefore, activists should be aware of the involved risks if they use personally-identifiable information (e.g. real name email addresses or permanent phone numbers), which might put them at risk, and take adequate measures to protect themselves, for example, by using anonymous email or phone numbers, so called "burner" contacts. Finally, the last menu item contains contact details if you want to get into contact with the DandelionDB Team using good ole' email or Twitter. We are looking forward to hearing from you!

Thank you for your interest in DandelionDB! Take care! :-)



