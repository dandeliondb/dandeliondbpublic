# README #

The "DandelionDB" database is an open-access database on civil-resistance actions
for science, democracy and a livable future.

### What is this repository for? ###

This is the code repository of "DandelionDB", which includes a web app frontend 
with a database backend to report civil resistance actions, especially related 
to the climate and ecological crises, but also other progressive movements based 
on science, democracy, and livable future in balance with our natural world.

### Disclaimer ###

This program is free software. You can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software Foundation;

This program is distributed in the hope that it will be useful, but
without any warranty, without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
General PublicLicense for more details. You can retrieve a copy of the
GNU public licence version 3 online (https://www.gnu.org/licenses/gpl-3.0.en.html).

### Who do I talk to? ###

* Contact: dandeliondb[at]pm.me